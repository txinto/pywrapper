def calculate_computed_return(function):
    if (function['lib'] == "ArcFitsFile"):
        ret = "computed_return = (pStatus == constants.ARC_FITS_STATUS_OK)"
    else:
        ret = "computed_return = (pStatus == constants.ARC_STATUS_OK)"

    return ret

def string2char_p(arg):
    return arg.encode('utf-8')

def keyval2void_p(arg):
    return eval(arg)

def status2and(arg):
    return ""

def status2ret(arg):
    return ""

def status2retfits(arg):
    return ""

def status2andfits(arg):
    return ""




