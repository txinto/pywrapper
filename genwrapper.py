# Importing auxiliar libraries for the test
import argparse                     # This library allows us to easily parse the command line arguments
from pyexcel_ods import get_data    # This function allows us to easily read an ODS file (for api)
from genwrapper_functions import *

# Importing test configuration file
import config

######### WE WILL PARSE THE COMMAND LINE ARGUMENTS FOR THE WRAPPER GEN #############
parser = argparse.ArgumentParser(description='Launches a wrapper generation from an ODS file describing the API to wrap')

## The second argument is the api ODS file
parser.add_argument('api_file',type=argparse.FileType('r'), help="the path of a file containing the API description")

# Obtaining the arguments from the command line
args=parser.parse_args()

# Printing the obtained arguments:
print("The API definition ODS filename is:",args.api_file.name)

# As an example of a constant defined in the configuration file, we'll print the welcome message
print(config.welcome_message)

# First we read the Types description data from the file
dictdata = get_data(args.api_file.name,start_row=config.api_file_start_row, row_limit=config.api_file_row_limit, 
    start_column=config.api_file_start_column,column_limit=config.api_file_column_limit)[config.api_file_dict_sheet]

# And we will create a dictionary with this data
types_dict = {}
for row in dictdata:
    if (len(row)>1):
        thiskey = row[config.api_file_dict_typename_column]
        if (len(thiskey)>0):
            #print(row)
            thistype = {}
            thistype['ctypes'] = row[config.api_file_dict_ctypes_column]
            thistype['isref'] = row[config.api_file_dict_isref_column]
            thistype['clang'] = row[config.api_file_dict_clang_column]

            if (len(row)>config.api_file_dict_encoder_column):
                if (row[config.api_file_dict_shallencode_column]):
                    thistype['encoder'] = row[config.api_file_dict_encoder_column]
                else:
                    thistype['encoder'] = None
            else:
                thistype['encoder'] = None
            
            if (len(row)>config.api_file_dict_default_column):
                thistype['default'] = str(row[config.api_file_dict_default_column])

            else:
                thistype['default'] = None


            types_dict[thiskey] = thistype
            #print(thiskey,thistype)


# Now we read the API description from the file file
functiondata = get_data(args.api_file.name,start_row=config.api_file_start_row, row_limit=config.api_file_row_limit, 
    start_column=config.api_file_start_column,column_limit=config.api_file_column_limit)[config.api_file_sheet]
# functiondata contains a dictionary with the spreadsheet, let's read it to a dictionary of api

functions_dict = {}
for row in functiondata:
    if (len(row)>1):
        thiskey = row[config.api_file_name_column]
        if (len(thiskey)>0):
            if (len(row)>config.api_file_include_column):
                if (row[config.api_file_include_column]):
                    #print(row)
                    thisfunc = {}
                    thisfunc['lib'] = row[config.api_file_library_column]
                    if len(row)> config.api_file_return_column:
                        ret = row[config.api_file_return_column]
                        if len(ret) > 0:
                            thisfunc['return'] = types_dict[ret]
                        else:
                            thisfunc['return'] = None
                    else:
                        thisfunc['return'] = None
                    
                    thisfunc['args'] = []
                    #print(thiskey)
                    for i in range(config.api_file_args_number):
                        #print(len(row))
                        #print(config.api_file_args_start_column+((i+1)*config.api_file_argset_column_size))
                        if len(row)>=config.api_file_args_start_column+((i+1)*config.api_file_argset_column_size):
                            thisargtype = row[config.api_file_args_start_column+(i*config.api_file_argset_column_size)+config.api_file_argset_type_column]
                            thisargdir = row[config.api_file_args_start_column+(i*config.api_file_argset_column_size)+config.api_file_argset_dir_column]
                            thisargname = row[config.api_file_args_start_column+(i*config.api_file_argset_column_size)+config.api_file_argset_name_column]
                            if (len(thisargtype)>0) and (len(thisargname)>0):
                                thisarg = {}
                                thisarg['name'] = thisargname
                                thisarg['type'] = types_dict[thisargtype]
                                if (len(thisargdir)>0):
                                    thisarg['dir'] = thisargdir
                                else:
                                    thisarg['dir'] = None

                                thisfunc['args'].append(thisarg)
                        else:
                            break

                    functions_dict[thiskey] = thisfunc
                    #print(thiskey,thisfunc)

print("########### TYPES READ: ######")
#print(types_dict)
print("########### FUNCTIONS READ: ######")
#print(functions_dict)
#print()
#print()
#print()
#print()

# Now we have parsed the input file, lets start playing rock'n'roll

for key in functions_dict.keys():
    print("#######",key,"#######")
    # Lets write its resttype
    # libArcFitsFile.ArcFitsFile_close.restype = None
    # Let's write the return of the function
    # #print("resultado de la llamada: ",retstatus)
    # return retstatus == constants.ARC_FITS_STATUS_OK
    restypestr = "lib"+functions_dict[key]['lib']+"."+key+".restype = "
    returnstr = "return "
    returndefstr = None
    functioncallstr = None
    need_computed_return = False
    if functions_dict[key]['return'] is None:
        restypestr += "None"
        if (len(functions_dict[key]['args'])>0):
            returndefstr = "computed_return = False"
            need_computed_return = True
            returnstr += "computed_return"
        
        functioncallstr = "lib"+functions_dict[key]['lib']+"."+key+"("
        
    else:
        restypestr += functions_dict[key]['return']['ctypes']
        functioncallstr = "ret = lib"+functions_dict[key]['lib']+"."+key+"("
        returnstr += "ret"

    print(restypestr)
    functions_dict[key]['restype'] = restypestr

    # Let's write its argtypes
    # libArcFitsFile.ArcFitsFile_close.argtypes = [ctypes.c_ulonglong, ctypes.POINTER(ctypes.c_int)]
    # Let's write the definition of the function
    # def ArcFitsFile_close(ulHandle):
    argtypesstr = "lib"+functions_dict[key]['lib']+"."+key+".argtypes = ["
    defstr = "def "+key+"("
    argnamelist = []
    argtypelist = []
    argcalllist = []
    argreflist = []
    argpostprocesslist = []
    argrefreturnlist = []
    argencodelist = []
    myargstrarglist = []
    outputargslist = []
    for arg in functions_dict[key]['args']:
        if (arg['dir'] is None) or (arg['dir'] != "OUTPUT"):
            argnamelist.append(arg['name'])
            
        else:
            outputargslist.append(arg['name']+" = "+arg['type']['default'])
        
        argcallstr = arg['name']
        myargstrarglist.append("'"+arg['name']+"':"+arg['name'])
        if (arg['type']['encoder'] is not None):
            argcallstr = arg['name']+"_enc"
            argencodelist.append(argcallstr+" = "+arg['type']['encoder']+"("+arg['name']+",myargs)")

        if (arg['type']['isref']):
            argtypelist.append("ctypes.POINTER("+arg['type']['ctypes']+")")
            argcall = "ctypes.byref("+argcallstr+"_p)"
            argreflist.append(arg['name']+"_p = "+arg['type']['ctypes']+"("+arg['name']+")")
            argpostprocesslist.append(arg['name']+" = "+arg['name']+"_p.value")
            argrefreturnlist.append(arg['name'])

        else:
            argtypelist.append(arg['type']['ctypes'])
            argcall = argcallstr

        argcalllist.append(argcall)

    myargsstr = "myargs = {"+','.join(myargstrarglist)+"}"
    argtypesstr += ','.join(argtypelist)+"]"
    argnamesstr = ','.join(argnamelist)
    argcallstr = ','.join(argcalllist)
    if (len(argrefreturnlist)>0):
        returnstr += ','+','.join(argrefreturnlist)

    defstr += argnamesstr + "):"
    functioncallstr += argcallstr + ")"

    functions_dict[key]['argtypes'] = argtypesstr
    functions_dict[key]['argnames'] = argnamesstr
    functions_dict[key]['def'] = defstr
    print(argtypesstr)
    print(defstr)
    for outputarg in outputargslist:
        print('\t'+outputarg)

    print('\t'+myargsstr)
    if (returndefstr is not None):
        print('\t'+returndefstr)
    
    for argrefstr in argreflist:
        print('\t'+argrefstr)
    
    for argenc in argencodelist:
        print('\t'+argenc)

    print('\t'+functioncallstr)
    print()

    for argpostprocess in argpostprocesslist:
        print('\t'+argpostprocess)

    if (need_computed_return):
        calcreturnstr = calculate_computed_return(functions_dict[key])
        if calcreturnstr is not None:
            print('\t'+calcreturnstr)
            print()
    
    print('\t'+returnstr)
    print()
    print()



    

