welcome_message = "HELLO!!!!.  We are about to wrap your API as you described in the Input ODS file.\n You owe a beer to cosmobots.eu"

api_file_start_row = 1
api_file_row_limit= 100
api_file_start_column = 0
api_file_column_limit = 40

api_file_sheet = "FuncDef"
api_file_library_column = 0
api_file_name_column = 1
api_file_include_column = 2
api_file_return_column = 3
api_file_args_start_column = 4
api_file_args_number = 12
api_file_argset_column_size = 3
api_file_argset_type_column = 0
api_file_argset_dir_column = 1
api_file_argset_name_column = 2

api_file_dict_sheet = "TypesDict"
api_file_dict_typename_column = 0
api_file_dict_ctypes_column = 1
api_file_dict_isref_column = 2
api_file_dict_clang_column = 3
api_file_dict_shallencode_column = 4
api_file_dict_encoder_column = 5
api_file_dict_default_column = 6
