The API definition ODS filename is: ./API_desc.ods
HELLO!!!!.  We are about to wrap your API as you described in the Input ODS file.
 You owe a beer to cosmobots.eu
########### TYPES READ: ######
########### FUNCTIONS READ: ######
####### ArcDevice_ToString #######
libArcDevice.ArcDevice_ToString.restype = ctypes.c_char_p
libArcDevice.ArcDevice_ToString.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_ToString():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_ToString(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_IsOpen #######
libArcDevice.ArcDevice_IsOpen.restype = ctypes.c_uint
libArcDevice.ArcDevice_IsOpen.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_IsOpen():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_IsOpen(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_Open #######
libArcDevice.ArcDevice_Open.restype = None
libArcDevice.ArcDevice_Open.argtypes = [ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_Open(uiDeviceNumber):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uiDeviceNumber':uiDeviceNumber,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_Open(uiDeviceNumber,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_Open_I #######
libArcDevice.ArcDevice_Open_I.restype = None
libArcDevice.ArcDevice_Open_I.argtypes = [ctypes.c_uint,ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_Open_I(uiDeviceNumber,uiBytes):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uiDeviceNumber':uiDeviceNumber,'uiBytes':uiBytes,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_Open_I(uiDeviceNumber,uiBytes,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_Open_II #######
libArcDevice.ArcDevice_Open_II.restype = None
libArcDevice.ArcDevice_Open_II.argtypes = [ctypes.c_uint,ctypes.c_uint,ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_Open_II(uiDeviceNumber,uiRows,uiCols):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uiDeviceNumber':uiDeviceNumber,'uiRows':uiRows,'uiCols':uiCols,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_Open_II(uiDeviceNumber,uiRows,uiCols,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_Close #######
libArcDevice.ArcDevice_Close.restype = None
libArcDevice.ArcDevice_Close.argtypes = []
def ArcDevice_Close():
	myargs = {}
	libArcDevice.ArcDevice_Close()

	return 


####### ArcDevice_Reset #######
libArcDevice.ArcDevice_Reset.restype = None
libArcDevice.ArcDevice_Reset.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_Reset():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_Reset(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_MapCommonBuffer #######
libArcDevice.ArcDevice_MapCommonBuffer.restype = None
libArcDevice.ArcDevice_MapCommonBuffer.argtypes = [ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_MapCommonBuffer(uiBytes):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uiBytes':uiBytes,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_MapCommonBuffer(uiBytes,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_UnMapCommonBuffer #######
libArcDevice.ArcDevice_UnMapCommonBuffer.restype = None
libArcDevice.ArcDevice_UnMapCommonBuffer.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_UnMapCommonBuffer():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_UnMapCommonBuffer(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_FillCommonBuffer #######
libArcDevice.ArcDevice_FillCommonBuffer.restype = None
libArcDevice.ArcDevice_FillCommonBuffer.argtypes = [ctypes.c_short,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_FillCommonBuffer(uwValue):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uwValue':uwValue,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_FillCommonBuffer(uwValue,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_CommonBufferVA #######
libArcDevice.ArcDevice_CommonBufferVA.restype = ctypes.c_void_p
libArcDevice.ArcDevice_CommonBufferVA.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_CommonBufferVA():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_CommonBufferVA(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_CommonBufferPA #######
libArcDevice.ArcDevice_CommonBufferPA.restype = ctypes.c_ulonglong
libArcDevice.ArcDevice_CommonBufferPA.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_CommonBufferPA():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_CommonBufferPA(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_CommonBufferSize #######
libArcDevice.ArcDevice_CommonBufferSize.restype = ctypes.c_ulonglong
libArcDevice.ArcDevice_CommonBufferSize.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_CommonBufferSize():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_CommonBufferSize(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_GetId #######
libArcDevice.ArcDevice_GetId.restype = ctypes.c_uint
libArcDevice.ArcDevice_GetId.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_GetId():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_GetId(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_GetStatus #######
libArcDevice.ArcDevice_GetStatus.restype = ctypes.c_uint
libArcDevice.ArcDevice_GetStatus.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_GetStatus():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_GetStatus(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_ClearStatus #######
libArcDevice.ArcDevice_ClearStatus.restype = None
libArcDevice.ArcDevice_ClearStatus.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_ClearStatus():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_ClearStatus(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_Set2xFOTransmitter #######
libArcDevice.ArcDevice_Set2xFOTransmitter.restype = None
libArcDevice.ArcDevice_Set2xFOTransmitter.argtypes = [ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_Set2xFOTransmitter(uiOnOff):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uiOnOff':uiOnOff,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_Set2xFOTransmitter(uiOnOff,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_LoadDeviceFile #######
libArcDevice.ArcDevice_LoadDeviceFile.restype = None
libArcDevice.ArcDevice_LoadDeviceFile.argtypes = [ctypes.c_char_p,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_LoadDeviceFile(pszFile):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pszFile':pszFile,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	pszFile_enc = string2char_p(pszFile,myargs)
	libArcDevice.ArcDevice_LoadDeviceFile(pszFile_enc,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_Command #######
libArcDevice.ArcDevice_Command.restype = ctypes.c_uint
libArcDevice.ArcDevice_Command.argtypes = [ctypes.c_uint,ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_Command(uiBoardId,uiCommand):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uiBoardId':uiBoardId,'uiCommand':uiCommand,'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_Command(uiBoardId,uiCommand,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_Command_I #######
libArcDevice.ArcDevice_Command_I.restype = ctypes.c_uint
libArcDevice.ArcDevice_Command_I.argtypes = [ctypes.c_uint,ctypes.c_uint,ctypes.POINTER(ctypes.c_int),ctypes.POINTER(ctypes.c_int)]
def ArcDevice_Command_I(uiBoardId,uiCommand):
	pStatus = constants.ARC_STATUS_OK
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uiBoardId':uiBoardId,'uiCommand':uiCommand,'pStatus':pStatus,'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_Command_I(uiBoardId,uiCommand,ctypes.byref(pStatus_p),ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	pStatus = pStatus_p.value
	return ret,pStatus,pStatus


####### ArcDevice_Command_II #######
libArcDevice.ArcDevice_Command_II.restype = ctypes.c_uint
libArcDevice.ArcDevice_Command_II.argtypes = [ctypes.c_uint,ctypes.c_uint,ctypes.POINTER(ctypes.c_int),ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_Command_II(uiBoardId,uiCommand,uiArg1):
	pStatus = constants.ARC_STATUS_OK
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uiBoardId':uiBoardId,'uiCommand':uiCommand,'pStatus':pStatus,'uiArg1':uiArg1,'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_Command_II(uiBoardId,uiCommand,ctypes.byref(pStatus_p),uiArg1,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	pStatus = pStatus_p.value
	return ret,pStatus,pStatus


####### ArcDevice_Command_III #######
libArcDevice.ArcDevice_Command_III.restype = ctypes.c_uint
libArcDevice.ArcDevice_Command_III.argtypes = [ctypes.c_uint,ctypes.c_uint,ctypes.POINTER(ctypes.c_int),ctypes.c_uint,ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_Command_III(uiBoardId,uiCommand,uiArg1,uiArg2):
	pStatus = constants.ARC_STATUS_OK
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uiBoardId':uiBoardId,'uiCommand':uiCommand,'pStatus':pStatus,'uiArg1':uiArg1,'uiArg2':uiArg2,'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_Command_III(uiBoardId,uiCommand,ctypes.byref(pStatus_p),uiArg1,uiArg2,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	pStatus = pStatus_p.value
	return ret,pStatus,pStatus


####### ArcDevice_Command_IIII #######
libArcDevice.ArcDevice_Command_IIII.restype = ctypes.c_uint
libArcDevice.ArcDevice_Command_IIII.argtypes = [ctypes.c_uint,ctypes.c_uint,ctypes.POINTER(ctypes.c_int),ctypes.c_uint,ctypes.c_uint,ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_Command_IIII(uiBoardId,uiCommand,uiArg1,uiArg2,uiArg3):
	pStatus = constants.ARC_STATUS_OK
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uiBoardId':uiBoardId,'uiCommand':uiCommand,'pStatus':pStatus,'uiArg1':uiArg1,'uiArg2':uiArg2,'uiArg3':uiArg3,'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_Command_IIII(uiBoardId,uiCommand,ctypes.byref(pStatus_p),uiArg1,uiArg2,uiArg3,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	pStatus = pStatus_p.value
	return ret,pStatus,pStatus


####### ArcDevice_GetControllerId #######
libArcDevice.ArcDevice_GetControllerId.restype = ctypes.c_uint
libArcDevice.ArcDevice_GetControllerId.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_GetControllerId():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_GetControllerId(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_ResetController #######
libArcDevice.ArcDevice_ResetController.restype = None
libArcDevice.ArcDevice_ResetController.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_ResetController():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_ResetController(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_IsControllerConnected #######
libArcDevice.ArcDevice_IsControllerConnected.restype = ctypes.c_uint
libArcDevice.ArcDevice_IsControllerConnected.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_IsControllerConnected():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_IsControllerConnected(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_SetupController #######
libArcDevice.ArcDevice_SetupController.restype = None
libArcDevice.ArcDevice_SetupController.argtypes = [ctypes.c_uint,ctypes.c_uint,ctypes.c_uint,ctypes.c_uint,ctypes.c_uint,ctypes.c_char_p,ctypes.c_char_p,ctypes.c_char_p,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_SetupController(uiReset,uiTDL,uiPower,uiRows,uiCols,pszTimFile,pszUtilFile,szPciFile):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uiReset':uiReset,'uiTDL':uiTDL,'uiPower':uiPower,'uiRows':uiRows,'uiCols':uiCols,'pszTimFile':pszTimFile,'pszUtilFile':pszUtilFile,'szPciFile':szPciFile,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	pszTimFile_enc = string2char_p(pszTimFile,myargs)
	pszUtilFile_enc = string2char_p(pszUtilFile,myargs)
	szPciFile_enc = string2char_p(szPciFile,myargs)
	libArcDevice.ArcDevice_SetupController(uiReset,uiTDL,uiPower,uiRows,uiCols,pszTimFile_enc,pszUtilFile_enc,szPciFile_enc,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_LoadControllerFile #######
libArcDevice.ArcDevice_LoadControllerFile.restype = None
libArcDevice.ArcDevice_LoadControllerFile.argtypes = [ctypes.c_char_p,ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_LoadControllerFile(pszFilename,uiValidate):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pszFilename':pszFilename,'uiValidate':uiValidate,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	pszFilename_enc = string2char_p(pszFilename,myargs)
	libArcDevice.ArcDevice_LoadControllerFile(pszFilename_enc,uiValidate,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_SetImageSize #######
libArcDevice.ArcDevice_SetImageSize.restype = None
libArcDevice.ArcDevice_SetImageSize.argtypes = [ctypes.c_uint,ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_SetImageSize(uiRows,uiCols):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uiRows':uiRows,'uiCols':uiCols,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_SetImageSize(uiRows,uiCols,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_GetImageRows #######
libArcDevice.ArcDevice_GetImageRows.restype = ctypes.c_uint
libArcDevice.ArcDevice_GetImageRows.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_GetImageRows():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_GetImageRows(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_GetImageCols #######
libArcDevice.ArcDevice_GetImageCols.restype = ctypes.c_uint
libArcDevice.ArcDevice_GetImageCols.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_GetImageCols():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_GetImageCols(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_GetCCParams #######
libArcDevice.ArcDevice_GetCCParams.restype = ctypes.c_uint
libArcDevice.ArcDevice_GetCCParams.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_GetCCParams():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_GetCCParams(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_IsCCParamSupported #######
libArcDevice.ArcDevice_IsCCParamSupported.restype = ctypes.c_uint
libArcDevice.ArcDevice_IsCCParamSupported.argtypes = [ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_IsCCParamSupported(uiParameter):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uiParameter':uiParameter,'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_IsCCParamSupported(uiParameter,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_IsCCD #######
libArcDevice.ArcDevice_IsCCD.restype = ctypes.c_uint
libArcDevice.ArcDevice_IsCCD.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_IsCCD():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_IsCCD(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_IsBinningSet #######
libArcDevice.ArcDevice_IsBinningSet.restype = ctypes.c_uint
libArcDevice.ArcDevice_IsBinningSet.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_IsBinningSet():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_IsBinningSet(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_SetBinning #######
libArcDevice.ArcDevice_SetBinning.restype = None
libArcDevice.ArcDevice_SetBinning.argtypes = [ctypes.c_uint,ctypes.c_uint,ctypes.c_uint,ctypes.c_uint,ctypes.POINTER(ctypes.c_uint),ctypes.POINTER(ctypes.c_uint),ctypes.POINTER(ctypes.c_int)]
def ArcDevice_SetBinning(uiRows,uiCols,uiRowFactor,uiColFactor):
	pBinRows = 0
	pBinCols = 0
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uiRows':uiRows,'uiCols':uiCols,'uiRowFactor':uiRowFactor,'uiColFactor':uiColFactor,'pBinRows':pBinRows,'pBinCols':pBinCols,'pStatus':pStatus}
	computed_return = False
	pBinRows_p = ctypes.c_uint(pBinRows)
	pBinCols_p = ctypes.c_uint(pBinCols)
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_SetBinning(uiRows,uiCols,uiRowFactor,uiColFactor,ctypes.byref(pBinRows_p),ctypes.byref(pBinCols_p),ctypes.byref(pStatus_p))

	pBinRows = pBinRows_p.value
	pBinCols = pBinCols_p.value
	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pBinRows,pBinCols,pStatus


####### ArcDevice_UnSetBinning #######
libArcDevice.ArcDevice_UnSetBinning.restype = None
libArcDevice.ArcDevice_UnSetBinning.argtypes = [ctypes.c_uint,ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_UnSetBinning(uiRows,uiCols):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uiRows':uiRows,'uiCols':uiCols,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_UnSetBinning(uiRows,uiCols,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_IsSyntheticImageMode #######
libArcDevice.ArcDevice_IsSyntheticImageMode.restype = ctypes.c_uint
libArcDevice.ArcDevice_IsSyntheticImageMode.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_IsSyntheticImageMode():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_IsSyntheticImageMode(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_SetOpenShutter #######
libArcDevice.ArcDevice_SetOpenShutter.restype = None
libArcDevice.ArcDevice_SetOpenShutter.argtypes = [ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_SetOpenShutter(uiShouldOpen):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uiShouldOpen':uiShouldOpen,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_SetOpenShutter(uiShouldOpen,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_Expose #######
libArcDevice.ArcDevice_Expose.restype = None
libArcDevice.ArcDevice_Expose.argtypes = [ctypes.c_float,ctypes.c_uint,ctypes.c_uint,pExposeCallType,pReadCallType,ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_Expose(fExpTime,uiRows,uiCols,pExposeCall,pReadCall,bOpenShutter):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'fExpTime':fExpTime,'uiRows':uiRows,'uiCols':uiCols,'pExposeCall':pExposeCall,'pReadCall':pReadCall,'bOpenShutter':bOpenShutter,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_Expose(fExpTime,uiRows,uiCols,pExposeCall,pReadCall,bOpenShutter,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_StopExposure #######
libArcDevice.ArcDevice_StopExposure.restype = None
libArcDevice.ArcDevice_StopExposure.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_StopExposure():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_StopExposure(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_Continuous #######
libArcDevice.ArcDevice_Continuous.restype = None
libArcDevice.ArcDevice_Continuous.argtypes = [ctypes.c_uint,ctypes.c_uint,ctypes.c_uint,ctypes.c_float,pFrameCallType,ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_Continuous(uiRows,uiCols,uiNumOfFrames,fExpTime,pFrameCall,bOpenShutter):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uiRows':uiRows,'uiCols':uiCols,'uiNumOfFrames':uiNumOfFrames,'fExpTime':fExpTime,'pFrameCall':pFrameCall,'bOpenShutter':bOpenShutter,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_Continuous(uiRows,uiCols,uiNumOfFrames,fExpTime,pFrameCall,bOpenShutter,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_StopContinuous #######
libArcDevice.ArcDevice_StopContinuous.restype = None
libArcDevice.ArcDevice_StopContinuous.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_StopContinuous():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_StopContinuous(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_IsReadout #######
libArcDevice.ArcDevice_IsReadout.restype = ctypes.c_uint
libArcDevice.ArcDevice_IsReadout.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_IsReadout():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_IsReadout(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_GetPixelCount #######
libArcDevice.ArcDevice_GetPixelCount.restype = ctypes.c_uint
libArcDevice.ArcDevice_GetPixelCount.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_GetPixelCount():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_GetPixelCount(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_GetCRPixelCount #######
libArcDevice.ArcDevice_GetCRPixelCount.restype = ctypes.c_uint
libArcDevice.ArcDevice_GetCRPixelCount.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_GetCRPixelCount():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_GetCRPixelCount(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_GetFrameCount #######
libArcDevice.ArcDevice_GetFrameCount.restype = ctypes.c_uint
libArcDevice.ArcDevice_GetFrameCount.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_GetFrameCount():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_GetFrameCount(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_ContainsError #######
libArcDevice.ArcDevice_ContainsError.restype = ctypes.c_uint
libArcDevice.ArcDevice_ContainsError.argtypes = [ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_ContainsError(uiWord):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uiWord':uiWord,'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_ContainsError(uiWord,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_ContainsError_I #######
libArcDevice.ArcDevice_ContainsError_I.restype = ctypes.c_uint
libArcDevice.ArcDevice_ContainsError_I.argtypes = [ctypes.c_uint,ctypes.c_uint,ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_ContainsError_I(uiWord,uiWordMin,uiWordMax):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uiWord':uiWord,'uiWordMin':uiWordMin,'uiWordMax':uiWordMax,'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_ContainsError_I(uiWord,uiWordMin,uiWordMax,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_GetNextLoggedCmd #######
libArcDevice.ArcDevice_GetNextLoggedCmd.restype = ctypes.c_char_p
libArcDevice.ArcDevice_GetNextLoggedCmd.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_GetNextLoggedCmd():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_GetNextLoggedCmd(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_GetLoggedCmdCount #######
libArcDevice.ArcDevice_GetLoggedCmdCount.restype = ctypes.c_uint
libArcDevice.ArcDevice_GetLoggedCmdCount.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_GetLoggedCmdCount():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_GetLoggedCmdCount(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_SetLogCmds #######
libArcDevice.ArcDevice_SetLogCmds.restype = None
libArcDevice.ArcDevice_SetLogCmds.argtypes = [ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_SetLogCmds(uiOnOff):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uiOnOff':uiOnOff,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_SetLogCmds(uiOnOff,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_GetArrayTemperature #######
libArcDevice.ArcDevice_GetArrayTemperature.restype = ctypes.c_double
libArcDevice.ArcDevice_GetArrayTemperature.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_GetArrayTemperature():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_GetArrayTemperature(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_GetArrayTemperatureDN #######
libArcDevice.ArcDevice_GetArrayTemperatureDN.restype = ctypes.c_double
libArcDevice.ArcDevice_GetArrayTemperatureDN.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_GetArrayTemperatureDN():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcDevice.ArcDevice_GetArrayTemperatureDN(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcDevice_SetArrayTemperature #######
libArcDevice.ArcDevice_SetArrayTemperature.restype = None
libArcDevice.ArcDevice_SetArrayTemperature.argtypes = [ctypes.c_double,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_SetArrayTemperature(gTempVal):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'gTempVal':gTempVal,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_SetArrayTemperature(gTempVal,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_LoadTemperatureCtrlData #######
libArcDevice.ArcDevice_LoadTemperatureCtrlData.restype = None
libArcDevice.ArcDevice_LoadTemperatureCtrlData.argtypes = [ctypes.c_char_p,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_LoadTemperatureCtrlData(pszFilename):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pszFilename':pszFilename,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	pszFilename_enc = string2char_p(pszFilename,myargs)
	libArcDevice.ArcDevice_LoadTemperatureCtrlData(pszFilename_enc,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_SaveTemperatureCtrlData #######
libArcDevice.ArcDevice_SaveTemperatureCtrlData.restype = None
libArcDevice.ArcDevice_SaveTemperatureCtrlData.argtypes = [ctypes.c_char_p,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_SaveTemperatureCtrlData(pszFilename):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pszFilename':pszFilename,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	pszFilename_enc = string2char_p(pszFilename,myargs)
	libArcDevice.ArcDevice_SaveTemperatureCtrlData(pszFilename_enc,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_FindDevices #######
libArcDevice.ArcDevice_FindDevices.restype = None
libArcDevice.ArcDevice_FindDevices.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDevice_FindDevices():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_FindDevices(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_DeviceCount #######
libArcDevice.ArcDevice_DeviceCount.restype = ctypes.c_uint
libArcDevice.ArcDevice_DeviceCount.argtypes = []
def ArcDevice_DeviceCount():
	myargs = {}
	ret = libArcDevice.ArcDevice_DeviceCount()

	return ret


####### ArcFitsFile_getInstance #######
libArcFitsFile.ArcFitsFile_getInstance.restype = ctypes.c_ulonglong
libArcFitsFile.ArcFitsFile_getInstance.argtypes = [ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcFitsFile_getInstance(uiBpp):
	pStatus = constants.ARC_FITS_STATUS_OK
	myargs = {'uiBpp':uiBpp,'pStatus':pStatus}
	pStatus_p = ctypes.c_int(pStatus)
	ret = libArcFitsFile.ArcFitsFile_getInstance(uiBpp,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	return ret,pStatus


####### ArcFitsFile_create #######
libArcFitsFile.ArcFitsFile_create.restype = None
libArcFitsFile.ArcFitsFile_create.argtypes = [ctypes.c_ulonglong,ctypes.c_char_p,ctypes.c_uint,ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcFitsFile_create(ulHandle,pszFileName,uiRows,uiCols):
	pStatus = constants.ARC_FITS_STATUS_OK
	myargs = {'ulHandle':ulHandle,'pszFileName':pszFileName,'uiRows':uiRows,'uiCols':uiCols,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	pszFileName_enc = string2char_p(pszFileName,myargs)
	libArcFitsFile.ArcFitsFile_create(ulHandle,pszFileName_enc,uiRows,uiCols,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_FITS_STATUS_OK)

	return computed_return,pStatus


####### ArcFitsFile_write #######
libArcFitsFile.ArcFitsFile_write.restype = None
libArcFitsFile.ArcFitsFile_write.argtypes = [ctypes.c_ulonglong,ctypes.c_void_p,ctypes.POINTER(ctypes.c_int)]
def ArcFitsFile_write(ulHandle,pBuf):
	pStatus = constants.ARC_FITS_STATUS_OK
	myargs = {'ulHandle':ulHandle,'pBuf':pBuf,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcFitsFile.ArcFitsFile_write(ulHandle,pBuf,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_FITS_STATUS_OK)

	return computed_return,pStatus


####### ArcFitsFile_writeKeyword #######
libArcFitsFile.ArcFitsFile_writeKeyword.restype = None
libArcFitsFile.ArcFitsFile_writeKeyword.argtypes = [ctypes.c_ulonglong,ctypes.c_char_p,ctypes.c_void_p,ctypes.c_uint,ctypes.c_char_p,ctypes.POINTER(ctypes.c_int)]
def ArcFitsFile_writeKeyword(ulHandle,pszKey,pKeyVal,uiValType,pszComment):
	pStatus = constants.ARC_FITS_STATUS_OK
	myargs = {'ulHandle':ulHandle,'pszKey':pszKey,'pKeyVal':pKeyVal,'uiValType':uiValType,'pszComment':pszComment,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	pszKey_enc = string2char_p(pszKey,myargs)
	pKeyVal_enc = keyval2void_p(pKeyVal,myargs)
	pszComment_enc = string2char_p(pszComment,myargs)
	libArcFitsFile.ArcFitsFile_writeKeyword(ulHandle,pszKey_enc,pKeyVal_enc,uiValType,pszComment_enc,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_FITS_STATUS_OK)

	return computed_return,pStatus


####### ArcFitsFile_close #######
libArcFitsFile.ArcFitsFile_close.restype = None
libArcFitsFile.ArcFitsFile_close.argtypes = [ctypes.c_ulonglong,ctypes.POINTER(ctypes.c_int)]
def ArcFitsFile_close(ulHandle):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'ulHandle':ulHandle,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcFitsFile.ArcFitsFile_close(ulHandle,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_FITS_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_GetDeviceStringList #######
libArcDevice.ArcDevice_GetDeviceStringList.restype = None
libArcDevice.ArcDevice_GetDeviceStringList.argtypes = []
def ArcDevice_GetDeviceStringList():
	myargs = {}
	libArcDevice.ArcDevice_GetDeviceStringList()

	return 


####### ArcDevice_FreeDeviceStringList #######
libArcDevice.ArcDevice_FreeDeviceStringList.restype = None
libArcDevice.ArcDevice_FreeDeviceStringList.argtypes = []
def ArcDevice_FreeDeviceStringList():
	myargs = {}
	libArcDevice.ArcDevice_FreeDeviceStringList()

	return 


####### ArcDevice_ReMapCommonBuffer #######
libArcDevice.ArcDevice_ReMapCommonBuffer.restype = None
libArcDevice.ArcDevice_ReMapCommonBuffer.argtypes = [ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_ReMapCommonBuffer(uiBytes):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uiBytes':uiBytes,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_ReMapCommonBuffer(uiBytes,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_SetSubArray #######
libArcDevice.ArcDevice_SetSubArray.restype = None
libArcDevice.ArcDevice_SetSubArray.argtypes = [ctypes.POINTER(ctypes.c_uint),ctypes.POINTER(ctypes.c_uint),ctypes.c_uint,ctypes.c_uint,ctypes.c_uint,ctypes.c_uint,ctypes.c_uint,ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_SetSubArray(uiRow,uiCol,uiSubRows,uiSubCols,uiBiasOffset,uiBiasWidth):
	pOldRows = 0
	pOldCols = 0
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pOldRows':pOldRows,'pOldCols':pOldCols,'uiRow':uiRow,'uiCol':uiCol,'uiSubRows':uiSubRows,'uiSubCols':uiSubCols,'uiBiasOffset':uiBiasOffset,'uiBiasWidth':uiBiasWidth,'pStatus':pStatus}
	computed_return = False
	pOldRows_p = ctypes.c_uint(pOldRows)
	pOldCols_p = ctypes.c_uint(pOldCols)
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_SetSubArray(ctypes.byref(pOldRows_p),ctypes.byref(pOldCols_p),uiRow,uiCol,uiSubRows,uiSubCols,uiBiasOffset,uiBiasWidth,ctypes.byref(pStatus_p))

	pOldRows = pOldRows_p.value
	pOldCols = pOldCols_p.value
	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pOldRows,pOldCols,pStatus


####### ArcDevice_UnSetSubArray #######
libArcDevice.ArcDevice_UnSetSubArray.restype = None
libArcDevice.ArcDevice_UnSetSubArray.argtypes = [ctypes.c_uint,ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_UnSetSubArray(uiRows,uiCols):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uiRows':uiRows,'uiCols':uiCols,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_UnSetSubArray(uiRows,uiCols,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_SetSyntheticImageMode #######
libArcDevice.ArcDevice_SetSyntheticImageMode.restype = None
libArcDevice.ArcDevice_SetSyntheticImageMode.argtypes = [ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcDevice_SetSyntheticImageMode(uiMode):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uiMode':uiMode,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDevice.ArcDevice_SetSyntheticImageMode(uiMode,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDevice_GetLastError #######
libArcDevice.ArcDevice_GetLastError.restype = ctypes.c_char_p
libArcDevice.ArcDevice_GetLastError.argtypes = []
def ArcDevice_GetLastError():
	myargs = {}
	ret = libArcDevice.ArcDevice_GetLastError()

	return ret


####### ArcDisplay_Launch #######
libArcDisplay.ArcDisplay_Launch.restype = None
libArcDisplay.ArcDisplay_Launch.argtypes = [ctypes.c_uint,ctypes.POINTER(ctypes.c_int)]
def ArcDisplay_Launch(uiMSDelay):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'uiMSDelay':uiMSDelay,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDisplay.ArcDisplay_Launch(uiMSDelay,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDisplay_ShowFits #######
libArcDisplay.ArcDisplay_ShowFits.restype = None
libArcDisplay.ArcDisplay_ShowFits.argtypes = [ctypes.c_char_p,ctypes.POINTER(ctypes.c_int)]
def ArcDisplay_ShowFits(pszFitsFile):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pszFitsFile':pszFitsFile,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	pszFitsFile_enc = string2char_p(pszFitsFile,myargs)
	libArcDisplay.ArcDisplay_ShowFits(pszFitsFile_enc,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDisplay_Clear #######
libArcDisplay.ArcDisplay_Clear.restype = None
libArcDisplay.ArcDisplay_Clear.argtypes = [ctypes.c_int,ctypes.POINTER(ctypes.c_int)]
def ArcDisplay_Clear(dFrame):
	pStatus = constants.ARC_STATUS_OK
	myargs = {'dFrame':dFrame,'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDisplay.ArcDisplay_Clear(dFrame,ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDisplay_Terminate #######
libArcDisplay.ArcDisplay_Terminate.restype = None
libArcDisplay.ArcDisplay_Terminate.argtypes = [ctypes.POINTER(ctypes.c_int)]
def ArcDisplay_Terminate():
	pStatus = constants.ARC_STATUS_OK
	myargs = {'pStatus':pStatus}
	computed_return = False
	pStatus_p = ctypes.c_int(pStatus)
	libArcDisplay.ArcDisplay_Terminate(ctypes.byref(pStatus_p))

	pStatus = pStatus_p.value
	computed_return = (pStatus == constants.ARC_STATUS_OK)

	return computed_return,pStatus


####### ArcDisplay_GetLastError #######
libArcDisplay.ArcDisplay_GetLastError.restype = ctypes.c_char_p
libArcDisplay.ArcDisplay_GetLastError.argtypes = []
def ArcDisplay_GetLastError():
	myargs = {}
	ret = libArcDisplay.ArcDisplay_GetLastError()

	return ret


